import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'login/:picture/:name/:email/:token', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'create', loadChildren: './pages/create/create.module#CreatePageModule' },
  { path: 'profile', loadChildren: './pages/profile/profile.module#ProfilePageModule' },
  { path: 'start', loadChildren: './pages/start/start.module#StartPageModule' },
  { path: 'fblogin', loadChildren: './pages/fblogin/fblogin.module#FbloginPageModule' },
  { path: 'select-cancha/:dataObj', loadChildren: './pages/select-cancha/select-cancha.module#SelectCanchaPageModule' },
  { path: 'add-cancha', loadChildren: './pages/add-cancha/add-cancha.module#AddCanchaPageModule' },
  { path: 'mymatches', loadChildren: './pages/mymatches/mymatches.module#MymatchesPageModule' },
  { path: 'joinmatch', loadChildren: './pages/joinmatch/joinmatch.module#JoinmatchPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
