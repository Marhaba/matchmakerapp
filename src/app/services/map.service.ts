import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LoadingController } from '@ionic/angular';

declare var google;

@Injectable({
    providedIn: 'root'
})

export class MapService {

    mapRef = null;

    constructor(
        private geolocation: Geolocation,
        private loadingCtrl: LoadingController
    ){ }

    public async loadMap(lat: number, lng: number) { 

        const loading = await this.loadingCtrl.create();
        loading.present();

        const myLatLng = await this.setLocation(lat, lng);

        const mapEle: HTMLElement = document.getElementById('map');
        this.mapRef = new google.maps.Map(mapEle, {
            center: myLatLng,
            zoom : 12
        });

        google.maps.event .addListenerOnce(this.mapRef, 'idle', () =>{
            loading.dismiss(); //apaga el cosito de esperar
            this.setLocationMarker(myLatLng.lat, myLatLng.lng);
            
        });

    }

    private async getLocation(){
        const rta = await this.geolocation.getCurrentPosition();
        return {
            lat: rta.coords.latitude,
            lng: rta.coords.longitude
        }
    }

    private async setLocation(latitud: number, longitud: number){
        return {
            lat: latitud,
            lng: longitud
        }
    }

    private setLocationMarker(lat: number,lng: number){
        const marker = new google.maps.Marker({ //crea el marcador
            position: {
            lat,
            lng
            },
            map: this.mapRef,
        })
    }
}
