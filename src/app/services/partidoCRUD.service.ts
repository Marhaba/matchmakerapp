import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PartidoI } from '../models/partido.interface';

@Injectable({
    providedIn: 'root'
})

export class PartidoCRUDService {
    private partidosCollection: AngularFirestoreCollection<PartidoI>;
    private partidos: Observable<PartidoI[]>;

    constructor(db: AngularFirestore) {
        this.partidosCollection = db.collection<PartidoI>('patidos');
    }

    getPartidos() {
        this.partidos = this.partidosCollection.snapshotChanges().pipe(map(
            actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;
                    return { id, ...data };
                });
            }
        ));
        return this.partidos;
    }
}