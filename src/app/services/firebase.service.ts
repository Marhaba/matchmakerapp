import { Injectable } from '@angular/core';
import { storage } from 'firebase';

@Injectable({
    providedIn: 'root'
})

export class FirebaseService {

    constructor() {}

    uploadImage(imageURI){
        return new Promise<any>((resolve, reject) => {
        let storageRef = storage().ref();
        let imageRef = storageRef.child('profilePictures');
        this.encodeImageUri(imageURI, function(image64){
          imageRef.putString(image64, 'data_url').then(snapshot => {
            resolve(snapshot.downloadURL)
          }, err => {
           reject(err);
          })
        })
      })
    }

    encodeImageUri(imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext("2d");
        var img = new Image();
        img.onload = function () {
          var aux:any = this;
          c.width = aux.width;
          c.height = aux.height;
          ctx.drawImage(img, 0, 0);
          var dataURL = c.toDataURL("image/jpeg");
          callback(dataURL);
        };
        img.src = imageUri;
      };

      getImage(id) {
        storage().ref(id).getDownloadURL().then(url => {
          console.log(url);
        });
      }
}