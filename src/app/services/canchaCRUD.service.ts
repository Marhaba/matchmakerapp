import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CanchaI } from '../models/cancha.interface';

@Injectable({
    providedIn: 'root'
})

export class CanchaCRUDService {
    private canchasCollection: AngularFirestoreCollection<CanchaI>;
    private canchas: Observable<CanchaI[]>;

    constructor(db: AngularFirestore) {
        this.canchasCollection = db.collection<CanchaI>('canchas');
    }

    getCanchas() {
        this.canchas = this.canchasCollection.snapshotChanges().pipe(map(
            actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;
                    return { id, ...data };
                });
            }
        ));
        return this.canchas;
    }

    getCancha(id: string) {
        return this.canchasCollection.doc<CanchaI>(id).valueChanges();
    }

    updateCancha(cancha: CanchaI, id: string) {
        return this.canchasCollection.doc(id).update(cancha);
    }

    addCancha(cancha: CanchaI) {
        return this.canchasCollection.add(cancha);
    }

    removeCancha(id: string) {
        return this.canchasCollection.doc(id).delete();
    }
}