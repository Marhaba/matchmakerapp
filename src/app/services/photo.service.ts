import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { storage } from 'firebase';
import { ImagePicker } from '@ionic-native/image-picker/ngx';

import { FirebaseService } from './firebase.service';

// import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})

export class PhotoService {

  // public photos: Photo[] = [];
  public base64Image = "/assets/logo.png";

  constructor(private camera: Camera, private imagePicker: ImagePicker, private firebaseService: FirebaseService) {}

  takePicture() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
    
    this.camera.getPicture(options).then((imageData) => {
      // Add new photo to gallery
      //this.photos.unshift({
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      //});

      let storageRef = storage().ref();
      let imageRef = storageRef.child('image').child('imageName');
      const pictures = storage().ref('profilePictures/wiwiwi');
      imageRef.putString(imageData, 'data_url');
      // Save all photos for later viewing
      // this.storage.set('photos', this.photos);
    }, (err) => {
     // Handle error
     console.log("Camera issue: " + err);
    });

  }

  openGallery() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation: true
    }
    
    this.camera.getPicture(options).then((imageData) => {
      this.base64Image = 'data:image/jpeg;base64,' + imageData;

      // Save photo to firebase
      let storageRef = storage().ref();
      let imageRef = storageRef.child('profilePictures').child('image');
      imageRef.putString(this.base64Image, 'data_url');

    }, (err) => {
     // Handle error
     console.log("Camera issue: " + err);
    });

  }

  openImagePicker(){
    this.imagePicker.hasReadPermission()
    .then((result) => {
      if(result == false){
        // no callbacks required as this opens a popup which returns
        this.imagePicker.requestReadPermission();
      }
      else if(result == true){
        this.imagePicker.getPictures({
          maximumImagesCount: 1
        })
        .then((results) => {
          for (var i = 0; i < results.length; i++) {
            this.uploadImageToFirebase(results[i]);
          }
        }, (err) => console.log(err));
      }
    }, (err) => {
      console.log(err);
    });
  }

  uploadImageToFirebase(image){
    //uploads img to firebase storage
    this.firebaseService.uploadImage(image);
  }

  /*
  loadSaved() {
    this.storage.get('photos').then((photos) => {
      this.photos = photos || [];
    });
  }
  */
}

class Photo {
  data: any;
}