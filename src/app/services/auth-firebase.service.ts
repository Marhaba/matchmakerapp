import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class AuthFirebaseService {

    isNewUserState = new BehaviorSubject(null);

    constructor(private afAuth: AngularFireAuth) {}

    login(credential) {
        try{
            this.afAuth.auth.signInWithCredential(credential).then(res => {
                if (res.additionalUserInfo.isNewUser)
                    this.isNewUserState.next(true);
                else
                    this.isNewUserState.next(false);

                return res.additionalUserInfo.username;
            });
        }
        catch(e){
            alert(e);
        }
    }

    userExists(username) {
        this.afAuth.auth.fetchSignInMethodsForEmail(username).then(res => {
            console.log('Length: ', res.length);
        });
    }
}