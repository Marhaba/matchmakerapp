import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MymatchesPage } from './mymatches.page';

describe('MymatchesPage', () => {
  let component: MymatchesPage;
  let fixture: ComponentFixture<MymatchesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MymatchesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MymatchesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
