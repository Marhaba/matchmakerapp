import { Component, OnInit } from '@angular/core';
import { PartidoI } from 'src/app/models/partido.interface';
import { PartidoCRUDService } from '../../services/partidoCRUD.service'

@Component({
  selector: 'app-mymatches',
  templateUrl: './mymatches.page.html',
  styleUrls: ['./mymatches.page.scss'],
})
export class MymatchesPage implements OnInit {
  partidos: PartidoI[];
  user = 'pollitos';

  constructor(
    private partidoCRUDService: PartidoCRUDService
  ) { }

  ngOnInit() {
    this.partidoCRUDService.getPartidos().subscribe(res => {
      this.partidos = res;
    });
  }

}
