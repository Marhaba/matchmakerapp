import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-start',
  templateUrl: './start.page.html',
  styleUrls: ['./start.page.scss'],
})
export class StartPage implements OnInit {
  
  constructor(private menuController: MenuController, private statusBar: StatusBar, private navController: NavController) {
    this.statusBar.backgroundColorByHexString('#EA7E00');
    this.menuController.enable(true);
  }

  ngOnInit() {
  }

  goCreatePage() {
    this.navController.navigateForward('create');
  }

  goJoinPage() {
    this.navController.navigateForward('joinmatch');
  }
}
