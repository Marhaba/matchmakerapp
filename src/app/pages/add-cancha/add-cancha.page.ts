import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-add-cancha',
  templateUrl: './add-cancha.page.html',
  styleUrls: ['./add-cancha.page.scss'],
})
export class AddCanchaPage implements OnInit {

  form = {
    name: "",
    desc: "",
    region: "",
    ciudad: "",
    direcc: "",
  };

  constructor(public alertController: AlertController, 
    private navController: NavController, 
    public toastController: ToastController,) {}

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Datos de cancha enviados',
      duration: 1800,
      
    });
    toast.present();
  }
  
  async presentAlert() {
    const alert = await this.alertController.create({
      message: '¿Desea enviar datos de cancha para ser revisados?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.navController.navigateRoot('start');
            this.presentToast();
          }
         
        }
      ]
    });

    await alert.present();
  }
  

  ngOnInit() {
  }

  logForm() {
    
  }

}
