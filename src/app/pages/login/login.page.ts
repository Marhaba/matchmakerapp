import { Component, OnInit } from '@angular/core';
import { MenuController, ToastController, NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { WheelSelector } from '@ionic-native/wheel-selector/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  private picture: string;
  private token: string;

  form = {
    name: "",
    email: "",
    city: "",
    paymentMethod: ""
  };

  textCityButton = "Seleccione ciudad...";

  pickerOptions = {
    
    level: [
      {description: "Principiante"},
      {description: "Intermedio"},
      {description: "Avanzado"}
    ],

    sport: [
      {description: "Fútbol"},
      {description: "Tenis"},
      {description: "Basquetbol"}
    ],

    city: [
      {description: "Valparaíso"},
      {description: "Viña del Mar"}
    ],

    paymentMethod: [
      {description: "Crédito"},
      {description: "Débito"}
    ]

  }

  constructor(
    private menu: MenuController, 
    private route: ActivatedRoute, 
    private wheelSelector: WheelSelector, 
    private toastController: ToastController,
    private statusBar: StatusBar, 
    private navController: NavController,
    private authService: AuthenticationService
    ) {

    this.statusBar.backgroundColorByHexString('#FFFFFF');
    this.menu.enable(false);
  }

  ngOnInit() {
    this.picture      = this.route.snapshot.paramMap.get('picture').replace(/@/g,'/');
    this.form.name    = this.route.snapshot.paramMap.get('name');
    this.form.email   = this.route.snapshot.paramMap.get('email');
    this.token        = this.route.snapshot.paramMap.get('token');
  }

  logForm() {
    this.authService.login(this.token);
    this.navController.navigateRoot('start');
  }

  openCityPicker() {
    this.wheelSelector.show({
      title: "Seleccione ciudad",
      positiveButtonText: "Elegir",
      negativeButtonText: "Atras",
      items: [this.pickerOptions.city]
    }).then(async result => {
      let msg = `Ciudad seleccionada: ${result[0].description}`;
      this.form.city = result[0].description;
    });
  }

  openPaymentMethodPicker() {
    this.wheelSelector.show({
      title: "Seleccione método de pago",
      positiveButtonText: "Elegir",
      negativeButtonText: "Atras",
      items: [this.pickerOptions.paymentMethod]
    }).then(async result => {
      let msg = `Método de pago seleccionado: ${result[0].description}`;
      this.form.paymentMethod = result[0].description;
    });
  }

  async openToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    await toast.present();
  }

}
