import { Component, OnInit } from '@angular/core';
import { MapService } from '../../services/map.service';
import { CanchaI } from '../../models/cancha.interface';
import { PartidoI } from '../../models/partido.interface';
import { ActivatedRoute } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import { map } from 'rxjs/operators';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-select-cancha',
  templateUrl: './select-cancha.page.html',
  styleUrls: ['./select-cancha.page.scss'],
})

export class SelectCanchaPage implements OnInit {
  
  cancha: CanchaI = null;
  times = ['11:00', '13:00', '15:00', '17:00', '19:00'];
  months = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];
  day: string;
  month: string;

  private partidosCollection: AngularFirestoreCollection<PartidoI>;

  constructor(
    private route: ActivatedRoute,
    private map: MapService,
    private datePicker: DatePicker,
    private db: AngularFirestore,
    public alertCtrl: AlertController,
    private navCtrl: NavController,
    private authService: AuthenticationService
    ) { 
      this.partidosCollection = db.collection<PartidoI>('patidos')
    }

  ngOnInit() {
    let dataRecv = this.route.snapshot.paramMap.get('dataObj').replace(/@/g,'\/').replace(/\*/g,'?').replace(/\_/g,'=');
    this.cancha = JSON.parse(dataRecv);
    this.map.loadMap(this.cancha.lat, this.cancha.lng);
  }

  chosenTime(time) {
    this.presentBookMatch(time);
  }

  pickDate() {
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      allowOldDates: false,
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {
        this.renderTimes(this.months[date.getMonth()], date.getDate().toString());
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  renderTimes(month: string, day: string) {
    this.month = month;
    this.day = day;

    this.partidosCollection.snapshotChanges().pipe(map(
      actions => {
          return actions.map(a => {
              const data = a.payload.doc.data();
              const id = a.payload.doc.id;
              return { id, ...data };
          });
      }
    )).subscribe(() => {
      var cont = 0;
      for (let t in this.times) {
        var partidos = this.db.collection('patidos');
        partidos.ref.where('idCancha', '==', this.cancha.id).where('month', '==', month).where('day', '==', day).where('time', '==', this.times[t]).get().then(res => {
          if(res.docs.length == 0)
            document.getElementById(t).style.display = "block";
          else {
            document.getElementById(t).style.display = "none";
            cont++;
          }
        }).then(() => {
          console.log('Contador: ', cont)
          if(cont == 5)
            this.presentAlertNoTimes();
        });
      }
    });

  }

  async presentAlertNoTimes() {
    const alert = await this.alertCtrl.create({
      header: 'Sin horarios disponibles',
      subHeader: '',
      message: 'Por favor, seleccione otro día.',
      buttons: ['OK'],
      mode: 'md',
      backdropDismiss: false
    });

    await alert.present();
  }

  async presentBookMatch(time: string) {
    const alert = await this.alertCtrl.create({
      header: 'Reserva tu cancha!',
      inputs: [
        {
          name: 'name1',
          type: 'text',
          placeholder: 'Nombre del partido'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: data => {
            this.bookMatch(data.name1, time);
          }
        }
      ]
    });

    await alert.present();
  }

  bookMatch(matchName: string, time: string) {
    this.authService.getToken().then(res => {
      var partido: PartidoI = {
        address: this.cancha.address,
        day: this.day,
        month: this.month,
        idCancha: this.cancha.id,
        name: matchName,
        players: '1',
        time: time,
        picture: this.cancha.picture.replace(/\/principal.png/, '%2Fprincipal.png'),
        user: res
      }
  
      this.db.collection('patidos').add(partido);
      this.presentDone();
      this.navCtrl.navigateRoot('start');
    })
  }

  hehe() {
    this.day = '19';
    this.month = 'septiembre';
    this.bookMatch('Hola', '11:00');
  }

  async presentDone() {
    const alert = await this.alertCtrl.create({
      header: 'Cancha reservada con éxito.',
      subHeader: '',
      buttons: ['OK'],
      mode: 'md',
      backdropDismiss: false
    });

    await alert.present();
  }
}
