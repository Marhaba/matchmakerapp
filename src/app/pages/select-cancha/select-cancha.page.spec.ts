import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectCanchaPage } from './select-cancha.page';

describe('SelectCanchaPage', () => {
  let component: SelectCanchaPage;
  let fixture: ComponentFixture<SelectCanchaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectCanchaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectCanchaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
