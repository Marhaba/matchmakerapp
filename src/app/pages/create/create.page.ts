import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { CanchaI } from '../../models/cancha.interface';
import { CanchaCRUDService } from '../../services/canchaCRUD.service';
import { LoadingController } from '@ionic/angular';

import { timer } from 'rxjs';

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})

export class CreatePage implements OnInit {
  canchas: CanchaI[];

  constructor(
    private canchaCRUDService: CanchaCRUDService, 
    private menuController: MenuController, 
    private statusBar: StatusBar, 
    private navController: NavController,
    private loadingCtrl: LoadingController
    ) {
    this.statusBar.backgroundColorByHexString('#EA7E00');
    this.menuController.enable(true);
  }

  async ngOnInit() {
    const loading = await this.loadingCtrl.create();
    loading.present();
    timer(4000).subscribe(() => {
      loading.dismiss();
    });
    
    this.canchaCRUDService.getCanchas().subscribe(res => {
      this.canchas = res;
    });
  }

  selectCancha(cancha: CanchaI) {
    let dataStr = JSON.stringify(cancha);
    this.navController.navigateForward('select-cancha/' + dataStr.replace(/\//g,'@').replace(/%2F/g,'@').replace(/\?/g,'*').replace(/\=/g,'_'));
  }
}