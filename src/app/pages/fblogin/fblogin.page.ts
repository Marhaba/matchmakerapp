import { Component, OnInit } from '@angular/core';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AlertController, MenuController, NavController } from '@ionic/angular';

import { AuthFirebaseService } from '../../services/auth-firebase.service';
import { auth } from 'firebase';

import { AuthenticationService } from '../../services/authentication.service'

@Component({
  selector: 'app-fblogin',
  templateUrl: './fblogin.page.html',
  styleUrls: ['./fblogin.page.scss'],
})
export class FbloginPage implements OnInit {

  userData = null;
  userToken = null;

  constructor(
    private facebook: Facebook, 
    private statusBar: StatusBar,
    public alertController: AlertController, 
    private menuController: MenuController, 
    private navController: NavController, 
    private authFirebaseService: AuthFirebaseService,
    private authService: AuthenticationService
    ) {

    this.statusBar.backgroundColorByHexString('#EA7E00');
    this.menuController.enable(false);
  }

  ngOnInit() {}

  goToLoginPage() {
    var picture = this.userData.picture.replace(/\//g,'@');
    let dataStr = JSON.stringify(this.userData);
    this.navController.navigateForward('select-cancha/' + picture + '/' + dataStr.replace(/\//g,'@').replace(/%2F/g,'@').replace(/\?/g,'*').replace(/\=/g,'_'))
  }

  logIn() {
    this.authService.login('pollitos');
    this.authFirebaseService.isNewUserState.subscribe(state => {
      console.log('isNewUser: ', state)
      switch (state) {
        case true:
          this.getFBData();
          this.navController.navigateRoot('start');
          this.authFirebaseService.isNewUserState.next(null);
          break;
        case false:
          this.navController.navigateRoot('start');
          this.authFirebaseService.isNewUserState.next(null);
          break;
      }
    })
  }

  go() {
    this.authService.login('ivanbeasc@hotmail.com');
    console.log('a: ', this.authService.getToken().then(res => {
      console.log('b: ', res)
    }));
    //this.navController.navigateForward('login/a/b/c');
  }

  loginWithFB() {
    this.facebook.login(['public_profile', 'email']).then((res: FacebookLoginResponse) => {
      this.userToken = this.authFirebaseService.login(auth.FacebookAuthProvider.credential(res.authResponse.accessToken));
    }).then(() => this.logIn()).catch(e => this.presentAlertFBAuthError());
  }

  getFBData() {
    this.facebook.api('me?fields=id,name,birthday,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
      this.userData =   {name: profile['name'], 
                        birthday: profile['birthday'], 
                        email: profile['email'], 
                        first_name: profile['first_name'], 
                        picture: profile['picture_large']['data']['url']};
    }).then(() => this.goToLoginPage());
  }

  async presentAlertFBAuthError() {
    const alert = await this.alertController.create({
      header: 'Error de autentificación',
      subHeader: '',
      message: 'Ha ocurrido un error al conseguir los datos desde Facebook. \n Por favor, intente nuevamente.',
      buttons: ['OK'],
      mode: 'md',
      backdropDismiss: false
    });

    await alert.present();
  }

}
