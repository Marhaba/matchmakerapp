import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FbloginPage } from './fblogin.page';

describe('FbloginPage', () => {
  let component: FbloginPage;
  let fixture: ComponentFixture<FbloginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FbloginPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FbloginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
