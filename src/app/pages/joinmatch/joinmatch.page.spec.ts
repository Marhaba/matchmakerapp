import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinmatchPage } from './joinmatch.page';

describe('JoinmatchPage', () => {
  let component: JoinmatchPage;
  let fixture: ComponentFixture<JoinmatchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinmatchPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinmatchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
