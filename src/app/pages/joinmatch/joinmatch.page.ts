import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { PartidoI } from '../../models/partido.interface';
import { PartidoCRUDService } from '../../services/partidoCRUD.service';
import { stringify } from '@angular/compiler/src/util';
import { AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-joinmatch',
  templateUrl: './joinmatch.page.html',
  styleUrls: ['./joinmatch.page.scss'],
})
export class JoinmatchPage implements OnInit {
  partidos: PartidoI[];
  

  constructor(private partidoCRUDService: PartidoCRUDService, 
              private menuController: MenuController, 
              private statusBar: StatusBar, 
              private navController: NavController,
              public alertController: AlertController,
              public toastController: ToastController) { 
    this.statusBar.backgroundColorByHexString('#EA7E00');
    this.menuController.enable(true);
  }

  ngOnInit() {
    this.partidoCRUDService.getPartidos().subscribe(res => this.partidos = res);
  }

  
  selectPartido(/*partido: PartidoI*/) {
    //let dataStr = JSON.stringify(partido);
    //this.navController.navigateForward('p-partido/'+ dataStr.replace(/\//g,'@').replace(/%2F/g,'@').replace(/\?/g,'*').replace(/\=/g,'_'));
    //this.navController.navigateForward('p-partido');
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Solicitud enviada',
      duration: 1800,
      
    });
    toast.present();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      message: '¿Desea solicitar invitación a partido?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'enviar',
          handler: () => {
            console.log('Confirm Okay');
            this.navController.navigateRoot('start');
            this.presentToast();
          }
        }
      ]
    });

    await alert.present();
  }
}
