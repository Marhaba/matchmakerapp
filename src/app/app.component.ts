import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavController } from '@ionic/angular';

import { timer } from 'rxjs';
import 'hammerjs';

import { AuthFirebaseService } from './services/auth-firebase.service';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})

export class AppComponent {

  showSplash = true;

  public appPages = [

    {
      title: 'Inicio',
      url: '/start',
      icon: 'home'
    },
    {
      title: 'Agregar Cancha',
      url: '/add-cancha',
      icon: 'football'
    },
    {
      title: 'Mis Partidos',
      url: '/mymatches',
      icon: 'logo-buffer'
    },
    {
      title: 'Log out',
      url: '/fblogin',
      icon: 'log-out'
    }
    
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authFirebaseService: AuthFirebaseService,
    private authService: AuthenticationService,
    private navController: NavController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      timer(3000).subscribe(() => this.showSplash = false);
    
      this.authService.authenticationState.subscribe(state => {
        console.log('Auth changed: ', state);
        switch (state) {
          case true:
            this.navController.navigateRoot('start');
            this.authService.authenticationState.next(null);
            break;
          case false: 
            this.navController.navigateRoot('fblogin');
            this.authService.authenticationState.next(null);
            break;
        }
      });
    });
  }

  logOut(title) {
    if(title == 'Log out') {
      this.navController.navigateRoot('fblogin');
      this.authService.logout();
    }
  }
}
