export interface PartidoI {
    id?: string; 
    address: string;
    day: string;
    month: string;
    idCancha: string;
    name: string;
    players: string;
    time: string;
    picture: string;
    user: string;
}