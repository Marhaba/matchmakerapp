export interface CanchaI {
    id?: string;
    name: string;
    address: string;
    owner: string;
    lat: number;
    lng: number;
    picture: string;
}